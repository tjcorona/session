//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/io/Logger.h"

#include "smtk/session/aeva/Registrar.h"

#include "smtk/session/aeva/operators/AllCellsFeature.h"
#include "smtk/session/aeva/operators/Delete.h"
#include "smtk/session/aeva/operators/Duplicate.h"
#include "smtk/session/aeva/operators/Export.h"
#include "smtk/session/aeva/operators/Import.h"
#include "smtk/session/aeva/operators/MeshLabel.h"
#include "smtk/session/aeva/operators/NormalFeature.h"
#include "smtk/session/aeva/operators/ProximityFeature.h"
#include "smtk/session/aeva/operators/Read.h"
#include "smtk/session/aeva/operators/Segment.h"
#include "smtk/session/aeva/operators/SelectionResponder.h"
#include "smtk/session/aeva/operators/Write.h"

#include "smtk/extension/paraview/server/VTKSelectionResponderGroup.h"

#include "smtk/geometry/Generator.h"
#include "smtk/session/aeva/Geometry.h"
#include "smtk/session/aeva/Resource.h"

#include "smtk/operation/groups/DeleterGroup.h"
#include "smtk/operation/groups/ExporterGroup.h"
#include "smtk/operation/groups/ImporterGroup.h"
#include "smtk/operation/groups/InternalGroup.h"
#include "smtk/operation/groups/ReaderGroup.h"
#include "smtk/operation/groups/WriterGroup.h"

#include "smtk/view/OperationIcons.h"

#include "smtk/session/aeva/all_cells_feature_opt_svg.h"
#include "smtk/session/aeva/normal_feature_opt_svg.h"
#include "smtk/session/aeva/proximity_feature_opt_svg.h"

namespace smtk
{
namespace session
{
namespace aeva
{

namespace
{
using OperationList = std::tuple<AllCellsFeature,
  Delete,
  Duplicate,
  Export,
  Import,
  MeshLabel,
  NormalFeature,
  ProximityFeature,
  Read,
  Segment,
  SelectionResponder,
  Write>;
}

class RegisterAEVAVTKBackend : public smtk::geometry::Supplier<RegisterAEVAVTKBackend>
{
public:
  bool valid(const Specification& in) const override
  {
    smtk::extension::vtk::geometry::Backend backend;
    return std::get<1>(in).index() == backend.index();
  }

  GeometryPtr operator()(const Specification& in) override
  {
    auto rsrc = std::dynamic_pointer_cast<smtk::session::aeva::Resource>(std::get<0>(in));
    if (rsrc)
    {
      auto provider = new Geometry(rsrc);
      return GeometryPtr(provider);
    }
    throw std::invalid_argument("Not an aeva resource.");
    return nullptr;
  }
};

void Registrar::registerTo(const smtk::resource::Manager::Ptr& resourceManager)
{
  // resource type to a manager
  resourceManager->registerResource<smtk::session::aeva::Resource>(read, write);
  RegisterAEVAVTKBackend::registerClass();
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  // Register operations to the operation manager
  operationManager->registerOperations<OperationList>();

  smtk::operation::ImporterGroup(operationManager)
    .registerOperation<smtk::session::aeva::Resource, smtk::session::aeva::Import>();

  smtk::view::VTKSelectionResponderGroup responders(operationManager, nullptr);
  responders.registerOperation<smtk::resource::Resource, smtk::session::aeva::SelectionResponder>();

  smtk::operation::InternalGroup internal(operationManager);
  internal.registerOperation<smtk::session::aeva::SelectionResponder>();

  smtk::operation::WriterGroup(operationManager)
    .registerOperation<smtk::session::aeva::Resource, smtk::session::aeva::Write>();
  smtk::operation::ReaderGroup(operationManager)
    .registerOperation<smtk::session::aeva::Resource, smtk::session::aeva::Read>();

  smtk::operation::DeleterGroup(operationManager).registerOperation<smtk::session::aeva::Delete>();
}

void Registrar::unregisterFrom(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->unregisterResource<smtk::session::aeva::Resource>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  smtk::operation::ImporterGroup(operationManager)
    .unregisterOperation<smtk::session::aeva::Import>();

  smtk::view::VTKSelectionResponderGroup responders(operationManager, nullptr);
  responders.unregisterOperation<smtk::session::aeva::SelectionResponder>();

  smtk::operation::InternalGroup internal(operationManager);
  internal.unregisterOperation<smtk::session::aeva::SelectionResponder>();

  smtk::operation::WriterGroup(operationManager).unregisterOperation<smtk::session::aeva::Write>();
  smtk::operation::ReaderGroup(operationManager).unregisterOperation<smtk::session::aeva::Read>();

  smtk::operation::DeleterGroup(operationManager)
    .unregisterOperation<smtk::session::aeva::Delete>();

  operationManager->unregisterOperations<OperationList>();
}

void Registrar::registerTo(const smtk::view::Manager::Ptr& viewManager)
{
  auto& opIcons(viewManager->operationIcons());
  opIcons.registerOperation<NormalFeature>(
    [](const std::string& color) { return normal_feature_opt_svg; });
  opIcons.registerOperation<ProximityFeature>(
    [](const std::string& color) { return proximity_feature_opt_svg; });
  opIcons.registerOperation<AllCellsFeature>(
    [](const std::string& color) { return all_cells_feature_opt_svg; });
}

void Registrar::unregisterFrom(const smtk::view::Manager::Ptr& viewManager)
{
  auto& opIcons(viewManager->operationIcons());
  opIcons.unregisterOperation<NormalFeature>();
  opIcons.unregisterOperation<ProximityFeature>();
  opIcons.unregisterOperation<AllCellsFeature>();
}

}
}
}
