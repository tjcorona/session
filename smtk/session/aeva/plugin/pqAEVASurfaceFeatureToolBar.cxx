//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/pqAEVASurfaceFeatureToolBar.h"

#include "smtk/session/aeva/CellSelection.h"
#include "smtk/session/aeva/Resource.h"
#include "smtk/session/aeva/Session.h"
#include "smtk/session/aeva/operators/AllCellsFeature.h"
#include "smtk/session/aeva/operators/NormalFeature.h"
#include "smtk/session/aeva/operators/ProximityFeature.h"
#include "smtk/session/aeva/plugin/pqFeatureReaction.h"

// SMTK
#include "smtk/view/Selection.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMSMTKWrapperProxy.h"

// VTK
#include "vtkDataSet.h"

// Qt
#include <QAction>
#include <QLabel>

static pqAEVASurfaceFeatureToolBar* s_surfaceFeatureToolBar = nullptr;

class pqAEVASurfaceFeatureToolBar::pqInternal
{
public:
  pqInternal(pqAEVASurfaceFeatureToolBar* toolbar)
  {
    QAction* nfr = new QAction("Select by surface normal", toolbar);
    nfr->setObjectName("NormalFeatureReaction");
    toolbar->addAction(nfr);
    m_normalFeatureReaction = new pqFeatureReaction<smtk::session::aeva::NormalFeature>(nfr);

    QAction* pfr = new QAction("Select by proximity", toolbar);
    pfr->setObjectName("ProximityFeatureReaction");
    toolbar->addAction(pfr);
    m_proximityFeatureReaction = new pqFeatureReaction<smtk::session::aeva::ProximityFeature>(pfr);

    m_cellSelectionSizeLabel = new QLabel("0 primitives selected", toolbar);
    toolbar->addWidget(m_cellSelectionSizeLabel);
  }

  ~pqInternal() {}

  pqFeatureReaction<smtk::session::aeva::ProximityFeature>* m_proximityFeatureReaction;
  pqFeatureReaction<smtk::session::aeva::NormalFeature>* m_normalFeatureReaction;
  QLabel* m_cellSelectionSizeLabel;
  smtk::view::SelectionObservers::Key m_selnObserver;
};

pqAEVASurfaceFeatureToolBar::pqAEVASurfaceFeatureToolBar(QWidget* parent)
  : Superclass(parent)
{
  m_p = new pqInternal(this);
  this->setObjectName("SurfaceFeatures");

  auto behavior = pqSMTKBehavior::instance();
  QObject::connect(behavior,
    SIGNAL(addedManagerOnServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(observeWrapper(pqSMTKWrapper*, pqServer*)));
  QObject::connect(behavior,
    SIGNAL(removingManagerFromServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(unobserveWrapper(pqSMTKWrapper*, pqServer*)));
  // Initialize with current wrapper(s), if any:
  behavior->visitResourceManagersOnServers([this](pqSMTKWrapper* wrapper, pqServer* server) {
    this->observeWrapper(wrapper, server);
    return false; // terminate early
  });

  if (!s_surfaceFeatureToolBar)
  {
    s_surfaceFeatureToolBar = this;
  }
}

pqAEVASurfaceFeatureToolBar::~pqAEVASurfaceFeatureToolBar()
{
  delete m_p;
  if (s_surfaceFeatureToolBar == this)
  {
    s_surfaceFeatureToolBar = nullptr;
  }
}

pqAEVASurfaceFeatureToolBar* pqAEVASurfaceFeatureToolBar::instance()
{
  return s_surfaceFeatureToolBar;
}

void pqAEVASurfaceFeatureToolBar::observeWrapper(pqSMTKWrapper* wrapper, pqServer*)
{
  m_p->m_selnObserver = wrapper->smtkSelection()->observers().insert(
    [this](const std::string& source, smtk::view::Selection::Ptr seln) {
      this->onSelectionChanged(source, seln);
    },
    std::numeric_limits<smtk::view::Selection::Observers::Priority>::lowest(),
    /* invoke observer on current selection */ true,
    "pqAEVASurfaceFeatureToolBar: Show primitive count.");
}

void pqAEVASurfaceFeatureToolBar::unobserveWrapper(pqSMTKWrapper* wrapper, pqServer*)
{
  m_p->m_selnObserver = smtk::view::SelectionObservers::Key();
  this->onPrimitivesSelected(nullptr);
}

void pqAEVASurfaceFeatureToolBar::onSelectionChanged(const std::string& selnSource,
  const std::shared_ptr<smtk::view::Selection>& seln)
{
  if (seln)
  {
    for (const auto& entry : seln->currentSelection())
    {
      if (entry.second)
      {
        auto prims = dynamic_cast<smtk::session::aeva::CellSelection*>(entry.first.get());
        if (prims)
        {
          this->onPrimitivesSelected(prims);
          return;
        }
      }
    }
  }
  this->onPrimitivesSelected(nullptr);
}

void pqAEVASurfaceFeatureToolBar::onPrimitivesSelected(smtk::session::aeva::CellSelection* cellSeln)
{
  std::ostringstream label;
  if (cellSeln)
  {
    auto rsrc = dynamic_cast<smtk::session::aeva::Resource*>(cellSeln->resource().get());
    if (rsrc)
    {
      vtkSmartPointer<vtkDataObject> obj = rsrc->session()->findStorage(cellSeln->id());
      auto dataset = vtkDataSet::SafeDownCast(obj);
      if (dataset)
      {
        label << dataset->GetNumberOfCells() << " primitives selected";
      }
      else if (obj)
      {
        label << obj->GetClassName() << " selected";
      }
    }
  }
  if (label.str().empty())
  {
    label << "0 primitives selected";
  }
  m_p->m_cellSelectionSizeLabel->setText(label.str().c_str());
}
