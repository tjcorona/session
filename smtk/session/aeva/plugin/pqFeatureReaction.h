//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_pqFeatureReaction_h
#define smtk_session_aeva_pqFeatureReaction_h

#include "pqReaction.h"

#include "smtk/view/Manager.h"
#include "smtk/view/OperationIcons.h"

#include "smtk/extension/qt/SVGIconEngine.h"

class pqSMTKWrapper;
class pqServer;

class pqFeatureReactionBase : public pqReaction
{
  Q_OBJECT
  using Superclass = pqReaction;

public:
  pqFeatureReactionBase(QAction* parent);
  ~pqFeatureReactionBase() override = default;

protected slots:
  void onWrapperAdded(pqSMTKWrapper* mgr, pqServer* server);

protected:
  void onTriggered() override;

  virtual std::string svg(const std::shared_ptr<smtk::view::Manager>& viewManager) const = 0;

  virtual std::shared_ptr<smtk::operation::Operation> operation(
    const std::shared_ptr<smtk::operation::Manager>& operationManager) const = 0;
};

template<typename FeatureOperation>
class pqFeatureReaction : public pqFeatureReactionBase
{
public:
  pqFeatureReaction(QAction* parent)
    : pqFeatureReactionBase(parent)
  {
  }

  std::string svg(const std::shared_ptr<smtk::view::Manager>& viewManager) const override
  {
    return viewManager->operationIcons().createIcon<FeatureOperation>("#ffffff");
  }

  std::shared_ptr<smtk::operation::Operation> operation(
    const std::shared_ptr<smtk::operation::Manager>& operationManager) const override
  {
    return operationManager->create<FeatureOperation>();
  }
};
#endif
