//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/pqAEVAShortcuts.h"

// #include "smtk/session/aeva/pqProximityFeatureReaction.h"
#include "smtk/session/aeva/CellSelection.h"
#include "smtk/session/aeva/Resource.h"
#include "smtk/session/aeva/Session.h"
#include "smtk/session/aeva/operators/Duplicate.h"

// SMTK
#include "smtk/view/Selection.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMSMTKWrapperProxy.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/operation/Manager.h"

// ParaView
#include "pqCoreUtilities.h"

// VTK
#include "vtkDataSet.h"

// Qt
#include <QShortcut>

static pqAEVAShortcuts* s_surfaceFeatureToolBar = nullptr;

class pqAEVAShortcuts::pqInternal
{
public:
  pqInternal(pqAEVAShortcuts* toolbar) {}

  ~pqInternal() {}

  smtk::view::SelectionObservers::Key m_selnObserver;
  QPointer<QShortcut> m_duplicateShortcut;
};

pqAEVAShortcuts::pqAEVAShortcuts(QObject* parent)
  : Superclass(parent)
{
  m_p = new pqInternal(this);

  auto mainWidget = pqCoreUtilities::mainWidget();
  m_p->m_duplicateShortcut = new QShortcut(
    QKeySequence("Ctrl+D"), mainWidget, nullptr, nullptr, Qt::WidgetWithChildrenShortcut);
  QObject::connect(m_p->m_duplicateShortcut, SIGNAL(activated()), this, SLOT(duplicateSelection()));

  if (!s_surfaceFeatureToolBar)
  {
    s_surfaceFeatureToolBar = this;
  }
}

pqAEVAShortcuts::~pqAEVAShortcuts()
{
  delete m_p;
  if (s_surfaceFeatureToolBar == this)
  {
    s_surfaceFeatureToolBar = nullptr;
  }
}

pqAEVAShortcuts* pqAEVAShortcuts::instance()
{
  return s_surfaceFeatureToolBar;
}

void pqAEVAShortcuts::duplicateSelection()
{
  auto behavior = pqSMTKBehavior::instance();
  auto wrapper = behavior->builtinOrActiveWrapper();
  if (wrapper)
  {
    auto seln = wrapper->smtkSelection();
    if (seln->currentSelection().empty())
    {
      smtkWarningMacro(smtk::io::Logger::instance(), "Selection empty; nothing to duplicate.");
      return;
    }
    auto operationManager = wrapper->smtkOperationManager();
    auto dupeOp = operationManager->create<smtk::session::aeva::Duplicate>();
    std::set<smtk::resource::Component::Ptr> selected;
    seln->currentSelectionByValue(selected, seln->selectionValueFromLabel("selected"), false);
    dupeOp->parameters()->associations()->setValues(selected.begin(), selected.end());
    operationManager->launchers()(dupeOp);
  }
  else
  {
    smtkWarningMacro(smtk::io::Logger::instance(), "Could not find SMTK wrapper.");
  }
}
