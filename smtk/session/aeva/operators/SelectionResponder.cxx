//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/operators/SelectionResponder.h"
#include "smtk/session/aeva/SelectionResponder_xml.h"

#include "smtk/extension/vtk/source/vtkResourceMultiBlockSource.h"

#include "smtk/view/Selection.h"

#include "smtk/session/aeva/CellSelection.h"
#include "smtk/session/aeva/Resource.h"

#include "smtk/model/Entity.h"
#include "smtk/model/Face.h"
#include "smtk/model/Model.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/operation/Manager.h"
#include "smtk/operation/MarkGeometry.h"

#include "smtk/io/Logger.h"

#include "vtkAppendDataSets.h"
#include "vtkCellData.h"
#include "vtkCompositeDataIterator.h"
#include "vtkExtractSelection.h"
#include "vtkIdTypeArray.h"
#include "vtkInformation.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkPolyData.h"
#include "vtkSelection.h"
#include "vtkSelectionNode.h"
#include "vtkUnsignedIntArray.h"

#ifdef ERROR
#undef ERROR
#endif

namespace smtk
{
namespace session
{
namespace aeva
{

SelectionResponder::SelectionResponder() = default;

SelectionResponder::~SelectionResponder() = default;

bool SelectionResponder::transcribeCellIdSelection(Result& result)
{
  bool didModify = false;
  if (this->selectingBlocks())
  {
    return didModify;
  }

  auto assoc = this->parameters()->associations();
  smtk::resource::ResourcePtr resource = assoc->valueAs<smtk::resource::Resource>();
  smtk::session::aeva::Resource::Ptr aevaResource =
    std::dynamic_pointer_cast<smtk::session::aeva::Resource>(resource);
  if (!resource)
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "No associated resource for aeva selection.");
    return false;
  }
  auto session = aevaResource->session();

  auto smtkSelection = this->smtkSelection();
  auto geometry = this->vtkData();
  ::vtkSelection* selnBlock = this->vtkSelection();
  vtkNew<vtkExtractSelection> extractor;
  extractor->SetInputDataObject(0, geometry);
  extractor->SetInputDataObject(1, selnBlock);
  // extractor->PreserveTopologyOn(); // <-- defines a selection array as point-data or cell-data
  extractor->Update();
  auto selectedStuff = vtkMultiBlockDataSet::SafeDownCast(extractor->GetOutputDataObject(0));

  auto created = result->findComponent("created");
  std::set<smtk::resource::ComponentPtr> selection;
  // Since both selectedStuff and geometry have the same structure,
  // we can apply the iterator to both.
  vtkCompositeDataIterator* iter = selectedStuff->NewIterator();
  // Users may have selected stuff from a side set. But aeva (at least for now)
  // only allows cell-selections from element blocks. So, instead of creating
  // a selection per iterator entry, we append all selected cells on a per-model
  // basis and create a "selection" per element-block entry.
  std::set<vtkSmartPointer<vtkDataObject> > selectedPieces;
  for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
  {
    auto seln = iter->GetCurrentDataObject();
    auto uuid = vtkResourceMultiBlockSource::GetDataObjectUUID(geometry->GetMetaData(iter));
    auto srcComp = aevaResource->findEntity(uuid);
    if (!srcComp)
    {
      continue;
    }
    selectedPieces.insert(seln);
  }
  iter->Delete();

  vtkSmartPointer<vtkDataObject> wholeSelection;
  if (!selectedPieces.empty())
  {
    if (selectedPieces.size() > 1)
    {
      vtkNew<vtkAppendDataSets> append;
      append->MergePointsOn();
      for (auto& input : selectedPieces)
      {
        append->AddInputData(input);
      }
      append->Update();
      wholeSelection = append->GetOutputDataObject(0);
    }
    else
    {
      wholeSelection = *selectedPieces.begin();
    }

    auto cellSelection = CellSelection::create(aevaResource, wholeSelection, this->manager());
    selection.insert(cellSelection);
    created->appendValue(cellSelection);
  }
  didModify |= smtkSelection->modifySelection(selection,
    m_smtkSelectionSource,
    m_smtkSelectionValue,
    view::SelectionAction::FILTERED_ADD,
    /* bitwise */ true,
    /* notify */ false);

  return didModify;
}

SelectionResponder::Result SelectionResponder::operateInternal()
{
  auto result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  bool worked = this->transcribeCellIdSelection(result);
  if (!worked)
  {
    result->findInt("outcome")->setValue(
      static_cast<int>(smtk::operation::Operation::Outcome::FAILED));
  }
  return result;
}

const char* SelectionResponder::xmlDescription() const
{
  return SelectionResponder_xml;
}

} // namespace aeva
} // namespace session
} // namespace smtk
