//=============================================================================
// Copyright (c) Kitware, Inc.
// All rights reserved.
// See LICENSE.txt for details.
//
// This software is distributed WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the above copyright notice for more information.
//=============================================================================

#include "smtk/io/Logger.h"

#include "smtk/session/aeva/operators/AllCellsFeature.h"

#include "smtk/model/Face.h"
#include "smtk/model/Model.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/operation/MarkGeometry.h"

#include "vtkAppendPolyData.h"
#include "vtkCellData.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkDoubleArray.h"
#include "vtkMath.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolyDataNormals.h"
#include "vtkThreshold.h"
#include "vtkVector.h"
#include "vtkVectorOperators.h"

#include <cmath>

#include "smtk/session/aeva/AllCellsFeature_xml.h"

// On Windows MSVC 2015+, something is included that defines
// a macro named ERROR to be 0. This causes smtkErrorMacro()
// to expand into garbage (because smtk::io::Logger::ERROR
// gets expanded to smtk::io::Logger::0).
#ifdef ERROR
#undef ERROR
#endif

using smtk::common::UUID;

namespace smtk
{
namespace session
{
namespace aeva
{

AllCellsFeature::Result AllCellsFeature::operateInternal()
{
  smtk::session::aeva::Resource::Ptr resource;
  smtk::session::aeva::Session::Ptr session;
  auto result = this->createResult(smtk::operation::Operation::Outcome::FAILED);
  this->prepareResourceAndSession(result, resource, session);

  auto assocs = this->parameters()->associations();
  auto created = result->findComponent("created");
  smtk::operation::MarkGeometry geomMarker(resource);
  std::vector<vtkSmartPointer<vtkPolyData> > pieces;
  smtk::model::Model anOwningModel;
  for (const auto& assoc : *assocs)
  {
    anOwningModel =
      smtk::model::Face(std::dynamic_pointer_cast<smtk::model::Entity>(assoc)).owningModel();
    auto data = session->findStorage(assoc->id());
    if (!data)
    {
      continue;
    }
    vtkSmartPointer<vtkPolyData> surf(vtkPolyData::SafeDownCast(data));
    if (!surf)
    {
      vtkNew<vtkDataSetSurfaceFilter> extractSurface;
      extractSurface->PassThroughCellIdsOn();
      extractSurface->SetInputDataObject(data);
      extractSurface->Update();
      surf = extractSurface->GetOutput();
    }
    if (!surf)
    {
      smtkErrorMacro(this->log(), "Could not generate surface of " << assoc->name() << ".");
      continue;
    }
    if (!surf->GetPointData()->GetNormals())
    {
      vtkNew<vtkPolyDataNormals> genNormals;
      genNormals->SetInputDataObject(surf);
      genNormals->SplittingOff();
      genNormals->ConsistencyOn();
      genNormals->SplittingOff();
      genNormals->ComputePointNormalsOn(); // Without this, rendering is super-flaky.
      genNormals->Update();
      surf = genNormals->GetOutput();
    }
    vtkNew<vtkPolyData> geom;
    geom->ShallowCopy(surf);
    pieces.push_back(geom);
  }

  if (!pieces.empty())
  {
    vtkNew<vtkAppendPolyData> append;
    for (const auto& piece : pieces)
    {
      append->AddInputData(piece.GetPointer());
    }
    append->Update();
    vtkNew<vtkPolyData> geom;
    geom->ShallowCopy(append->GetOutput());

    auto face = resource->addFace();
    face.setName("all boundary cells");
    anOwningModel.addCell(face);
    auto fcomp = face.entityRecord();
    session->addStorage(fcomp->id(), geom);
    created->appendValue(fcomp);
    geomMarker.markModified(fcomp);

    result->findInt("outcome")->setValue(static_cast<int>(AllCellsFeature::Outcome::SUCCEEDED));
  }
  else
  {
    smtkErrorMacro(this->log(), "No output.");
  }

  return result;
}

const char* AllCellsFeature::xmlDescription() const
{
  return AllCellsFeature_xml;
}

}
}
}
