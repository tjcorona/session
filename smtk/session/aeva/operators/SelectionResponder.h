//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_SelectionResponder_h
#define smtk_session_aeva_SelectionResponder_h
#ifndef __VTK_WRAP__

#include "smtk/extension/paraview/server/RespondToVTKSelection.h"
#include "smtk/session/aeva/Exports.h" // For export macro

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief An operation that handles cell selections on mesh resources.
  *
  * This operation, invoked with a vtkSelection dataset, creates a
  * new meshset and adds it to the SMTK selection when VTK cell indices
  * are provided.
  */
class SMTKAEVASESSION_EXPORT SelectionResponder : public smtk::view::RespondToVTKSelection
{
public:
  using Result = smtk::operation::Operation::Result;
  smtkTypeMacro(SelectionResponder);
  smtkCreateMacro(SelectionResponder);
  smtkSharedFromThisMacro(smtk::view::RespondToVTKSelection);
  smtkSuperclassMacro(smtk::view::RespondToVTKSelection);
  virtual ~SelectionResponder();

protected:
  SelectionResponder();

  /**\brief A convenience method that subclasses may use internally
    *       to handle VTK index selections.
    *
    * This will create a new polydata object bound to a new cells
    * and select it.
    *
    * Note that the selection is filtered.
    */
  bool transcribeCellIdSelection(Result& result);

  /// Simply call transcribeCellIdSelection().
  Result operateInternal() override;

private:
  const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif
#endif // smtk_session_aeva_SelectionResponder_h
