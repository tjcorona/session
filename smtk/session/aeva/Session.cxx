//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/session/aeva/Session.h"

#include "vtkDataObject.h"

namespace smtk
{
namespace session
{
namespace aeva
{

std::string Session::defaultFileExtension(const smtk::model::Model&) const
{
  return ".vti";
}

vtkSmartPointer<vtkDataObject> Session::findStorage(const smtk::common::UUID& uid) const
{
  auto it = m_storage.find(uid);
  if (it != m_storage.end())
  {
    return it->second;
  }
  return nullptr;
}

void Session::addStorage(const smtk::common::UUID& uid, vtkSmartPointer<vtkDataObject> storage)
{
  if (uid.isNull())
  {
    return;
  }

  if (!storage)
  {
    m_storage.erase(uid);
  }
  else
  {
    m_storage[uid] = storage;
  }
}

#if 0
Session::ImageType::Pointer Session::findStorage(const smtk::common::UUID& uid) const
{
  auto it = m_storage.find(uid);
  if (it != m_storage.end())
  {
    return it->second;
  }
  return nullptr;
}

void Session::addStorage(const smtk::common::UUID& uid, ImageType::Pointer storage)
{
  if (uid.isNull())
  {
    return;
  }

  if (!storage)
  {
    m_storage.erase(uid);
  }
  else
  {
    m_storage[uid] = storage;
  }
}
#endif

bool Session::removeStorage(const smtk::common::UUID& uid)
{
  if (uid.isNull())
  {
    return false;
  }

  return m_storage.erase(uid) > 0;
}

smtk::model::SessionInfoBits Session::transcribeInternal(const smtk::model::EntityRef& entity,
  SessionInfoBits requestedInfo,
  int depth)
{
  return smtk::model::SESSION_EVERYTHING;
}

smtk::model::SessionIOPtr Session::createIODelegate(const std::string& format)
{
  // TODO: If we write any custom JSON to .smtk files, return a handler.
  return nullptr;
}

} // namespace aeva
} // namespace session
} // namespace smtk
