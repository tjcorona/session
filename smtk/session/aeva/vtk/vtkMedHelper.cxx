//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "vtkMedHelper.h"
#include "vtkObjectFactory.h"

HdfNode* HdfNode::findChild(const std::string& name)
{
  for (std::list<HdfNode*>::iterator i = children.begin(); i != children.end(); i++)
  {
    if ((*i)->name == name)
      return *i;
  }
  return nullptr;
}

static herr_t buildTree(hid_t locId, const char* name, const H5L_info_t* info, void* opData)
{
  const std::string strName = name;
  H5O_info_t infoBuffer;
  hid_t status = H5Oget_info_by_name(locId, name, &infoBuffer, H5P_DEFAULT);

  // Create a node in the tree for this, doubly link it
  HdfNode* parentNode = static_cast<HdfNode*>(opData);
  HdfNode* currNode = new HdfNode{ locId, name, parentNode->path + name + '/' };
  parentNode->children.push_back(currNode);
  currNode->parent = parentNode;

  // Test what the link is pointing too
  if (infoBuffer.type == H5O_TYPE_GROUP)
    status = H5Literate_by_name(
      locId, name, H5_INDEX_NAME, H5_ITER_NATIVE, NULL, buildTree, currNode, H5P_DEFAULT);

  return 0;
}

extern HdfNode* rootBuildTree(hid_t rootId)
{
  HdfNode* rootNode = new HdfNode{ rootId, ".", "./" };
  H5Literate(rootId, H5_INDEX_NAME, H5_ITER_NATIVE, NULL, buildTree, rootNode);
  return rootNode;
}

void deleteTree(HdfNode* node)
{
  for (HdfNodeIterator i = node->children.begin(); i != node->children.end(); i++)
  {
    deleteTree(*i);
  }
  delete node;
}
